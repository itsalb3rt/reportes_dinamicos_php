<?php
try{
	$base=new PDO('mysql:host=localhost; dbname=ssfraudes', 'root', '');
	
	$base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	$base->exec("SET CHARACTER SET UTF8");

	$sql= "SELECT datos_solicitudes.id
	FROM   `datos_solicitudes` 
	INNER JOIN datos_usuarios 
	ON datos_solicitudes.id_solicitante = datos_usuarios.id	   
	INNER JOIN sectores 
	ON datos_solicitudes.id_sector = sectores.id 
	INNER JOIN utilitario.provincias 
	ON datos_solicitudes.id_provincia = provincias.id_provincia 
	INNER JOIN utilitario.municipios 
	ON datos_solicitudes.id_municipio = municipios.id_municipio
	INNER JOIN utilitario.secciones 
	ON datos_solicitudes.id_seccion = secciones.id_seccion 
	INNER JOIN utilitario.localidades 
	ON datos_solicitudes.id_localidad_barrio = localidades.id_localidad
	INNER JOIN fraudes_electricos
	ON datos_solicitudes.id = fraudes_electricos.id_solicitud
	INNER JOIN estados_solicitudes
	ON fraudes_electricos.id_estado = estados_solicitudes.id
	INNER JOIN entidades_comerciales 
	ON datos_solicitudes.id = entidades_comerciales.id_solicitud WHERE datos_solicitudes.fecha_creacion BETWEEN :nfecha_inicio AND :nfecha_final";
	$registros=$base->prepare($sql);									 	
	$registros->execute(array(':nfecha_inicio'=>$fecha_inicio,':nfecha_final'=>$fecha_final));
	$registro_obtenido = $registros->rowCount();
	$detalles_filas = $registros->fetchAll( PDO::FETCH_NUM );

	/**
	*Obteniendo el nombre de las columnas para posteriormente asignarlos a un arreglo
	**/
	foreach ( range( 0, $registros->columnCount() - 1 ) as $column_index ) {
		$meta[] = $registros->getColumnMeta( $column_index );
	}
	/**
	*Obteniendo el numero de columnas para escribirlas en la tabla,
	*se resta 1 porque el metodo count devuelve el conteno iniciando en 1 
	**/
	$x = count($meta)-1; 
	echo '<table width="100" border="1"><thead><tr>'; //Imprimiendo el encabezado de la tabla
	/**
	*Recorriendo los encabezados de la tabla
	**/
	for($i = 0; $i <= $x; $i++)
		echo "<th>".$meta[$i]['name']."</th>";

	echo "</tr></thead><tbody>" ;
	/**
	*campos de la consulta dinamicos, esto dependeran de los resultados de la consulta.
	* El primer ciclo recorre la cantidad de filas devueltas
	* El segundo ciclo recorre todos los valores de cada columna de una fila n
	* va desde n1 hasta nm.
	**/
	$x = count($detalles_filas[0])-1; //Cantidad de campos devueltos por la consulta,se resta 1 porque el metodo count devuelve el conteno iniciando en 1 
	foreach($detalles_filas as $keys=>$valor){
		echo "<tr>";
		for($i = 0; $i <= $x;$i++ )
			echo "<td>$valor[$i]</td>";
		echo "</tr>";
	}
	//cerrando la tabla
	echo "</tbody></table>";
	
}catch (Exception $e){
	
	die('Error ' . $e->getMessage());
	echo "Linea del error" . $e->getLine();
	
}
?>